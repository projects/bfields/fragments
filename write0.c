#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int fd, ret;

	if (argc != 2)
		errx(1, "need 1 integer argument");
	fd = open(argv[1], O_RDWR);
	if (fd < 0)
		err(1, "open");
	ret = write(fd, NULL, 0);
	if (ret < 0)
		err(1, "write");
	close(fd);
}
