#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
	char *big = "abcdefghijklmnopqrstuvwxyz0123456789";
	char *small = "foo";
	char proc[128];
	char link[4096];
	int fdbig, fdsmall, ret;

	fdbig = open(big, O_CREAT|O_RDWR, 0666);
	if (fdbig == -1)
		err("open");
	fdsmall = open(small, O_CREAT|O_RDWR, 0666);
	if (fdsmall == -1)
		err("open");
	close(fdsmall);
	ret = rename(small, big);
	if (ret)
		err("rename");
	sprintf(proc, "/proc/%d/fd/%d", getpid(), fdbig);
	ret = readlink(proc, link, sizeof(link));
	if (ret < 0)
		err("readlink");
	link[ret] = '\0';
	printf("%s\n", link);
}
