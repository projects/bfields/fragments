#define _GNU_SOURCE
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <unistd.h>

static loff_t
copy_file_range(int fd_in, loff_t *off_in, int fd_out,
		loff_t *off_out, size_t len, unsigned int flags)
{
	return syscall(__NR_copy_file_range, fd_in, off_in, fd_out,
                          off_out, len, flags);
}


int main(int argc, char *argv[])
{
	int fd_in, fd_out;
	loff_t len, ret;


	loff_t offset = 10;
	size_t len = (size_t)(-11);

	printf("sizeof(loff_t) = %d\n", sizeof(offset));
	printf("sizeof(size_t) = %d\n", sizeof(len));
	printf("(long long)(%llx + %lld) = %lld\n", offset, b, (long long)(a+b));

	if (argc != 3) {
		fprintf(stderr, "Usage: %s <source> <destination>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	fd_in = open(argv[1], O_RDONLY);
	if (fd_in == -1) {
		perror("open (argv[1])");
		exit(EXIT_FAILURE);
	}

	fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);
	if (fd_out == -1) {
		perror("open (argv[2])");
		exit(EXIT_FAILURE);
	}

	ret = copy_file_range(fd_in, &a, fd_out, NULL, len, 0);
		if (ret == -1) {
			perror("copy_file_range");
			exit(EXIT_FAILURE);
		}

		len -= ret;
	} while (len > 0);

}
