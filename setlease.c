#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <err.h>

int sigio = 0;

void simple_sighand(int sig)
{
	sigio = 1;
}

void setup_sighand()
{
	struct sigaction sa = {
		.sa_handler = simple_sighand,
	};
	int ret;

	ret = sigaction(SIGIO, &sa, NULL);
	if (ret)
		err(1, "sigaction");
}

void usage(char *comm)
{
	errx(1, "usage: %s r|w <path> ", comm);
}

int main(int argc, char *argv[])
{
	int fd, ret;
	unsigned int mode;
	char *path, *modestr;

	if (argc != 3)
		usage(argv[0]);
	modestr = argv[1];
	path = argv[2];
	if (!strcmp(modestr, "r"))
		mode = F_RDLCK;
	else if (!strcmp(modestr, "w"))
		mode = F_WRLCK;
	else
		usage(argv[0]);

	fd = open(path, O_RDONLY|O_CREAT, 0666);
	if (fd == -1)
		err(1, "open");
	setup_sighand();
	ret = fcntl(fd, F_SETLEASE, mode);
	if (ret == -1)
		err(1, "fcntl");
	printf("got lease; waiting\n");
	ret = select(0, NULL, NULL, NULL, NULL);
	if (!sigio)
		err(1, "select");
	printf("got sigio, waiting\n");
	ret = select(0, NULL, NULL, NULL, NULL);
}
