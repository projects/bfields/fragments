#include <sys/types.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char *argv[])
{
	char buf[4] = "\0\0\0\0";
	int ret;

	if (argc != 2)
		errx(1, "usage: %s path", argv[0]);
	
	ret = setxattr(argv[1], "system.nfs4_acl", buf, sizeof(buf), 0);
	exit(ret);
}
