#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <err.h>

#define LOCK_MAND 32

void usage(char *comm)
{
	err(1, "%s (r|w)[m] <path>", comm);
}

int main(int argc, char *argv[])
{
	char *f;
	char *opstr;
	int op;
	int fd, ret;

	if (argc != 3)
		usage(argv[0]);

	opstr = argv[1];
	switch (opstr[0]) {
	case 'r':
		op = LOCK_SH;
		break;
	case 'w':
		op = LOCK_EX;
		break;
	default:
		usage(argv[0]);
	}
	switch (opstr[1]) {
	case 'm':
		op |= LOCK_MAND;
	case '\0':
		break;
	default:
		usage(argv[0]);
	}

	fd = open(argv[2], O_RDWR);
	if (fd == -1)
		err(1, "open");
	ret = flock(fd, op);
	if (ret)
		err(1, "flock");
	printf("got lock\n");
	sleep(1000);
}
