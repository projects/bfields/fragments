#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int ret;
	struct stat st;

	if (argc != 2)
		errx(1, "usage: %s filename\n", argv[0]);
	ret = stat(argv[1], &st);
	printf("mode: %o\n", st.st_mode);
	exit(0);
}
