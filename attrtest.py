#!/usr/bin/env python

import sys
import os
sys.path.insert(1, os.path.join(sys.path[0], 'nfs4.1'))
import rpc
from rpc.security import AuthSys, AuthGss
from nfs4client import NFS4Client
from xdrdef.nfs4_const import *
from xdrdef.nfs4_type import *
import nfs_ops

op = nfs_ops.NFS4ops()

sec = rpc.security.instance(rpc.AUTH_SYS)
cred = sec.init_cred(uid=0, gid=0, name='test2')

c = NFS4Client(host='test1', minorversion=2)

c.set_cred(cred)

s = c.new_client_session('bar')

res = s.compound([op.putrootfh(), op.getattr(1 << FATTR4_CHANGE_ATTR_TYPE)])

res = s.compound([op.putrootfh(), op.lookup('exports'), op.lookup('xfs'),
                  op.getattr(1 << FATTR4_CHANGE_ATTR_TYPE)])
