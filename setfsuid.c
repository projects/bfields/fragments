#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>

int main(int argc, char *argv[])
{
	struct passwd *pw;
	uid_t uid;
	pid_t pid;
	int ret;

	if (argc != 3) {
		printf("usage: %s user pid\n", argv[0]);
		exit(-1);
	}
	pw = getpwnam(argv[1]);
	if (pw == NULL)
		err("\n");
	uid = pw->pw_uid;
	pid = atoi(argv[2]);
	printf("setting fsuid to %d\n", uid);
	ret = setfsuid(uid);
	printf("touching FOOFOO\n");
	ret = open("FOOFOO", O_CREAT);
	if (ret == -1)
		warn("open");
	printf("sending SIGKILL to %d\n", pid);
	ret = kill(pid, SIGKILL);
	if (ret = -1)
		warn("kill");
}
