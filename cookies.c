#include <dirent.h>
#include <stdio.h>
#include <errno.h>
#include <err.h>

int main(int argc, char *argv[])
{
	struct dirent *de;
	DIR *d;
	int ret;
	long c;

	d = opendir("foo");
	if (!d)
		err(1, "opendir(foo)");
	while (de = readdir(d)) {
		c = telldir(d);
		if (c == -1)
			err(1, "telldir");
		ret = closedir(d);
		if (ret == -1)
			err(1, "closedir");
		d = opendir("foo");
		if (!d)
			err(1, "opendir(foo)");
		seekdir(d, c);
	}
	if (!de && errno)
		err(1, "readdir");
}

