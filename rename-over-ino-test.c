#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>

void print_ino(char *p)
{
	struct stat b;
	int rc;

	rc = stat(p, &b);
	if (rc)
		warn("stat %s", p);
	else
		printf("%s: %i\n", p, b.st_ino);
}

void fprint_ino(char *s, int fd)
{
	struct stat b;
	int rc;

	rc = fstat(fd, &b);
	if (rc)
		warn("fstat %s", s);
	else
		printf("%s: %i\n", s, b.st_ino);
}

int
main(int argc, char **argv)
{
    int rc = 0;
    int f1, f2;
    char *fname1;
    char *fname2;

    if (argc != 3) {
        printf("Usage: %s <file1> <file2>\n", argv[0]);
        exit(1);
    }

    fname1 = argv[1];
    fname2 = argv[2];

    rc = mkdir(fname1, 0666);
    if (rc < 0)
	err(1, "mkdir(%s)", fname1);
    f1 = open(fname1, O_RDONLY);
    if (rc < 0)
	err(1, "open(%s)", fname1);
    fprint_ino(fname1, f1);

    rc = mkdir(fname2, 0666);
    if (rc < 0)
	err(1, "mkdir(%s)", fname2);
    f2 = open(fname2, O_RDONLY);
    if (rc < 0)
        err(1, "open(%s)", fname2);
    fprint_ino(fname2, f2);

    rc = rename(fname1, fname2);
    if (rc < 0)
        err(1, "rename");

    fprint_ino(fname1, f1);
    fprint_ino(fname2, f2);
    // print_ino(fname1);
    // print_ino(fname2);

    rc = access(fname1, F_OK);
    if (rc < 0) {
        printf("success\n");
        exit(1);
    }

    printf("file %s exists!\n", fname1);

    return 0;
}
