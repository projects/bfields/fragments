#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(void)
{
	struct flock rdlock = { F_RDLCK, SEEK_SET, 0, 0, 0 };
	struct flock wrlock = { F_WRLCK, SEEK_SET, 0, 0, 0 };
	char buff[4096] = { 0 };
	int fd;

	fd = open("/nfs/testfile", O_RDONLY);
	fcntl(fd, F_SETLK, &rdlock);

	fd = open("/nfs/testfile", O_RDWR);
	fcntl(fd, F_SETLK, &wrlock);
	write(fd, buff, sizeof buff);
	close(fd);
	return 0;
}
