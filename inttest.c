#include <stdio.h>

int main(int argc, char *argv[])
{
	int i;

	sscanf(argv[1], "%4d", &i);

	printf("int %d, short %d\n", i, (unsigned short)i);
}
