#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/syscall.h>

int main(int argc, char *argv[])
{
	struct file_handle *fh = NULL;
	int mtid;
	int ret;
	int dirfd;
	int fd;
	int fdlen = 0;
	char *name;
	struct stat st;

	if (argc != 2)
		errx(1, "usage: %s <path to filesystem>", argv[0]);

	name = argv[1];
	dirfd = open(name, O_RDONLY);
	if (dirfd == -1)
		errx(1, "open(%s)", name);

	do {
		fh = realloc(fh, sizeof(struct file_handle) +
						fdlen + MAX_HANDLE_SZ);
		if (!fh)
			errx(1, "out of memory");
		ret = read(0, fh + fdlen, MAX_HANDLE_SZ);
		if (ret == -1)
			err(1, "read");
		fdlen += ret;
	} while (ret == MAX_HANDLE_SZ);

	fd = open_by_handle_at(dirfd, fh, O_RDONLY);
	if (fd == -1)
		err(1, "open_by_handle_at");
	ret = fstat(fd, &st);
	if (ret == -1)
		err(1, "stat");
	printf("got fd for ino %ld size %ld\n", st.st_ino, st.st_size);
	exit(0);
}
