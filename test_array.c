#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	char a[100];
	char *b = a;

	printf("a = %p, &a = %p\n", a, &a);
	printf("b = %p, &b = %p\n", b, &b);
	b = &a;
	exit(0);
}
