#include <stdio.h>

int main(int argc, char *argv[])
{
	long i = -1;
	unsigned long u;
	unsigned int ui = 5;
	int ii = 3;

	printf("%ld\n", i);
	u = i;
	printf("%lu\n", u);
	printf("%ld\n", (long)u);

	printf("%d > %u - 6 = %d\n", ii, ui, ii > ui - 6);
}
