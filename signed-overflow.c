#include <stdio.h>
#include <limits.h>

int main(int argc, char *argv[])
{
	int i = INT_MAX;
	int j = INT_MAX;
	printf("i == j == INT_MAX\n");

	printf("i = j = %d (%x), i+j = %d, (u)i+(u)u = %d (%x)\n",
		i, j, i+j, (int)((unsigned int)i + (unsigned int)j),
		(unsigned int)i + (unsigned int)j);

	printf("LONG_MAX + 1 = %ld (%lx)\n", LONG_MAX + 1L, LONG_MAX + 1L);

	/* miscellaneous limits: */
	printf(" INT_MAX %x,  LONG_MAX %lx\n", INT_MAX, LONG_MAX);
	printf("UINT_MAX %x, ULONG_MAX %lx\n", UINT_MAX, ULONG_MAX);
}
