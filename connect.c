#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <err.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	struct hostent *he;
	struct sockaddr_in laddr, raddr, after;
	int afterlen;
	int so, ret;
	int buflen = 1000;
	char buf[buflen];
	const char *retbuf;

	if (argc != 3)
		errx(1, "usage: %s host port\n", argv[0]);

	he = gethostbyname(argv[1]);
	if (he == NULL)
		err(1, "gethostbyname");

	if (he->h_addrtype != AF_INET)
		errx(1, "non-IP address??");

	if (he->h_length != 4)
		errx(1, "address length %d??\n", he->h_length);

	raddr.sin_family = AF_INET;
	raddr.sin_port = htons(atoi(argv[2]));
	memcpy(&raddr.sin_addr, he->h_addr_list[0], sizeof(struct in_addr));

	so = socket(PF_INET, SOCK_STREAM, 0);
	if (so < 0)
		err(1, "socket");
	laddr.sin_family = AF_INET;
	laddr.sin_port = 0;
	laddr.sin_addr.s_addr = htonl(INADDR_ANY);
	ret = bind(so, (struct sockaddr *)&laddr, sizeof(laddr));
	if (ret < 0)
		err(1, "bind");
	ret = connect(so, (struct sockaddr *)&raddr, sizeof(raddr));
	if (ret < 0)
		err(1, "connect");
	afterlen = sizeof(struct sockaddr_in);
	ret = getsockname(so, (struct sockaddr *)&after, &afterlen);
	if (ret < 0)
		err(1, "getsockname");
	if (afterlen != sizeof(struct sockaddr_in))
		errx(1, "address length %d??\n", afterlen);
	retbuf = inet_ntop(AF_INET, &after.sin_addr, buf, buflen);
	if (retbuf == NULL)
		err(1, "inet_ntop");

	printf("connected from %s.%d\n", buf, ntohs(after.sin_port));
	select(0, NULL, NULL, NULL, NULL);
}
