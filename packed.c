#include <stdio.h>

struct unpacked {
	char c;
	int i;
};

struct __attribute__ ((__packed__)) packed {
	char c;
	int i;
};

int main(int argc, char *argv[])
{
	printf("unpacked.i at %p\n", &((struct unpacked *)0)->i);
	printf("  packed.i at %p\n", &((struct packed *)0)->i);
}
