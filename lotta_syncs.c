#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <utime.h>
#include <stdio.h>
#include <err.h>
#include <unistd.h>
#include <stdlib.h>

void usage(char *prog)
{
	errx(1, "usage: %s <path to file> <number of operations>", prog);
}

int main(int argc, char *argv[])
{
	struct timespec t[2];
	/* struct stat s; */
	char *file;
	int fd;
	int i, n;

	t[0].tv_nsec = UTIME_OMIT;
	t[1].tv_sec = 0;
	t[1].tv_nsec = 0;
	if (argc != 3)
		usage(argv[0]);
	file = argv[1];
	n = strtol(argv[2], NULL, 0);
	fd = open(file, O_RDWR, 0666); 
	if (fd == -1)
		err(1, "open");
	for (i=0; n == 0 || i<n; i++) {
		futimens(fd, t);
		fsync(fd);
		t[1].tv_nsec++;
		/* fstat(fd, &s);
		printf("%lu\n", s.st_mtim.tv_nsec); */
	}
}
