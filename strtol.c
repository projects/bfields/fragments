#include <stdio.h>
#include <stdlib.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int i;
	unsigned int u;
	long l;
	unsigned long ul;

	if (argc != 2)
		errx(1, "%s <number>", argv[0]);
	i = strtol(argv[1], NULL, 0);
	u = strtol(argv[1], NULL, 0);
	l = strtol(argv[1], NULL, 0);
	ul = strtol(argv[1], NULL, 0);
	printf("i = %d, u = %u, l = %ld, ul = %lu\n", i, u, l, ul);
	ul = strtoul(argv[1], NULL, 0);
	printf("ul = %lu\n", ul);
	i = ul;
	printf("i = %d\n", u);
}
