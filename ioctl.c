#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <err.h>
#include <linux/fs.h>

int main(int argc, char *argv[])
{
	int fd, ret;
	long long size;

	if (argc != 2)
		errx(1, "need 1 pathname argument");
	fd = open(argv[1], O_RDWR);
	if (fd == -1)
		err(1, "open");
	ret = ioctl(fd, BLKGETSIZE64, &size);
	if (ret == -1)
		err(1, "ioctl");
	printf("size: %lld\n", size);
}
