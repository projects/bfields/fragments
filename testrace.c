#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>

void usage(char *comm)
{
	errx(1, "usage: %s <path>", comm);
}

#define DIR "TRACEME_DIR"

int main(int argc, char *argv[])
{
	struct stat s;
	int ret;

	if (argc != 2)
		usage(argv[0]);
	ret = chdir(argv[1]);
	if (ret)
		err(1, "unable to chdir to %s\n", argv[1]);
	ret = mkdir(DIR, 0777);
	if (ret && errno != EEXIST)
		err(1, "mkdir");
	ret = open(DIR "/foo", O_CREAT|O_RDONLY);
	if (ret == -1)
		err(1, "open");
	close(ret);
	ret = fork();
	if (ret == -1)
		err(1, "fork");
	if (ret == 0) { /* child */
		/* Note this rmdir will block for a second: */
		ret = rmdir(DIR);
		if (ret != -1 || errno != ENOTEMPTY)
			err(1, "rmdir");
		exit(0);
	}
	/* half-second to give child a chance to attempt rmdir: */
	ret = usleep(500 * 1000);
	if (ret)
		err(1, "usleep");
	ret = fork();
	if (ret == -1)
		err(1, "second fork");
	if (ret == 0) { /* child */
		/*
		 * Note nfsd should map the fh for the first argument
		 * right away, but then will wait a second before
		 * mapping the fh in the second argument:
		 */
		ret = rename(DIR "/foo", DIR "/TRACEME-foo");
		if (ret)
			err(1, "rename");
		exit(0);
	}
	/*
	 * This triggers a lookup of DIR.  Note it should block
	 * another half-second since rmdir is still waiting with the
	 * i_mutex on DIR.  It will therefore arrive between the two
	 * filehandle mappings in the rename above:
	 */
	ret = stat(DIR, &s);
	if (ret)
		err(1, "stat");
}
