#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int ret, fd;

	ret = mkdir("FOO", 0777);
	if (ret)
		err(1, "mkdir");
	fd = open("FOO", O_RDONLY);
	if (fd == -1)
		err(1, "open");
	ret = rmdir("FOO");
	if (ret)
		err(1, "rmdir");
	sleep(INT_MAX);
}
