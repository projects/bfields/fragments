#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <err.h>

int do_pathconf(int param)
{
	int ret;

	errno = 0;
	ret = pathconf(".", param);
	if (ret == -1 && errno)
		err(1, "pathconf");
	return ret;
}

int main(int argc, char *argv[])
{
	int ret;

	ret = do_pathconf(_PC_NAME_MAX);
	printf("_PC_NAME_MAX: %d\n", ret);
	ret = do_pathconf(_PC_PATH_MAX);
	printf("_PC_PATH_MAX: %d\n", ret);
}
