#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int fd, ret;
	int i;

	if (argc != 2)
		errx(1, "need a path");
	fd = open(argv[1], O_RDWR);
	if (fd < 0)
		err(1, "open(%s)", argv[1]);
	for (i=0; i < 40960; i++) {
		ret = pwrite(fd, "x", 1, i * 4096);
		if (ret != 1)
			err(1, "write");
	}
	close(fd);
}
