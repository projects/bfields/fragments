#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int fd;

	fd = open(argv[1], O_RDONLY|O_TRUNC);
	if (fd == 0)
		err(1, "open");
}
