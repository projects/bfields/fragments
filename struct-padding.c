#include <stdio.h>
#include <stddef.h>

int main(int argc, char *argv[])
{
	struct abc {
		char a;
		int b;
		long c;
		double d;
	} s;
	
	printf("%d %d %d\n", offsetof(struct abc, a),
			     offsetof(struct abc, b),
			     offsetof(struct abc, c));
	printf("%d %d %d %d %d\n", sizeof(s.a), sizeof(s.b), sizeof(s.c),
			sizeof(s.d), sizeof(s));
}
