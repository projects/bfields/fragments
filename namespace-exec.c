#define _GNU_SOURCE
#include <unistd.h>
#include <signal.h>
#include <sched.h>
#include <sys/syscall.h>
#include <err.h>

/* Can demonstrate network namespace with e.g.
 * ./namespace-exec /bin/bash
 * In parent, create dummy0 with "modprobe dummy", then
 * ip link set dummy0 netns <pid of forked process>
 * and then you should see dummy0 in the cloned process but not
 * elsewhere
 *
 * Also fun to replace NEWNET by NEWNS and do mounts and see that
 * they're only visible in the cloned process.
 */

int main(int argc, char *argv[])
{
	int pid;
	int flags = SIGCHLD;

	if (argc < 2)
		errx(1, "usage: %s command arg1 arg2 ...", argv[0]);
	flags |= CLONE_NEWNS|CLONE_NEWPID;
	pid = syscall(__NR_clone, flags, NULL);
	if (pid < 0)
		err(1, "clone failed");
	if (pid == 0) {
		/* child */
		int ret;

		ret = execv(argv[1], argv+1);
		if (ret)
			err(1,"failed to exec %s", argv[1]);
	}
	wait();
}
