#include <unistd.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int fd;
	int ret;
	char b;

	while (read(0, &b, 1)) {
		if (b >= 128)
			continue;
		if (b == 0x0d)
			b = '\n';
		else if (b < 32 && b != '\t' && b != '\n')
			continue;
		ret = write(1, &b, 1);
		if (ret != 1)
			err(1, "write");
	}
}
