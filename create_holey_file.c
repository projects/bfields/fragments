#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	off_t size, ret;
	int fd;

	if (argc != 3)
		err(1, "usage: %s path order", argv[0]);
	size = 1 << atoi(argv[2]);
	fd = open(argv[1], O_CREAT | O_WRONLY | O_TRUNC, 0644);
	if (fd < 0)
		err(1, "open");
	ret = ftruncate(fd, size);
	if (ret < 0)
		err(1, "ftruncate");
	close(fd);
}
