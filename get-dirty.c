#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <err.h>
#include <sched.h>
#include <sys/mman.h>

/* time delta histogram to measure jitter:
 * cut -b1-14 TMP | ( read f; while read g; do echo "$g - $f"; f=$g; done ) | bc |sort |uniq -c
 */
/* Testing with simultaneous dd if=/dev/zero of=BIGFILE bs=1M
 *  till it filled up laptop drive (about 9.5 GB, about 3 minutes).
 */
/* On separate (usb!) fs; slightly longer run:
      1 .001
      1 .004
      7 .005
      4 .006
      3 .007
     77 .008
   6408 .009
  18773 .010
   6411 .011
     79 .012
      3 .013
      3 .014
      6 .015
      1 .017
      1 .019
 * On fs shared with dd'er:
      2 .003
      1 .004
      2 .005
      3 .006
      5 .007
     54 .008
   4524 .009
   8114 .010
   4540 .011
     44 .012
      6 .013
      2 .014
      1 .015
      1 .016
      2 .017
      1 .020
 */


int get_dirty(void)
{
	const bufsize = 4096;
	char buf[bufsize];
	int dirty;
	FILE *f;
	char *s;
	int ret;

	buf[bufsize-1] = '\0';
	f = fopen("/proc/meminfo", "r");
	if (!f)
		err(1, "open");
	while (s = fgets(buf, bufsize - 1, f)) {
		if (!memcmp("Dirty:", buf, strlen("Dirty:")))
			break;
	}
	if (!s)
		err(1, "Dirty not found\n");
	ret = sscanf(buf, "Dirty: %d", &dirty);
	if (ret != 1)
		err(1, "Parse error");
	fclose(f);
	return dirty;
}

/* assumes ms < 1000 : */
void add_ms(struct timespec *a, int ms)
{
	a->tv_sec += ms / 1000;
	ms %= 1000;
	a->tv_nsec += ms * 1000 * 1000;
	if (a->tv_nsec >= 1000 * 1000 * 1000) {
		a->tv_nsec -= 1000 * 1000 * 1000;
		a->tv_sec++;
	}
}

int diff_ms(struct timespec *a, struct timespec *b)
{
	int ms;

	ms = (a->tv_nsec - b->tv_nsec) / (1000 * 1000);
	ms += (a->tv_sec - b->tv_sec) * 1000;
	return ms >= 1 ? ms : 1;
}

void usage(char *comm)
{
	errx(1, "%s <millisecond interval (default 20)>", comm);
}

/* Unclear this really helps much, but what the heck: */
void attempt_realtime(void)
{
	struct sched_param sp = {
		.sched_priority = 1
	};
	int ret;

	ret = sched_setscheduler(0, SCHED_FIFO, &sp);
	if (ret == -1)
		warn("realtime scheduling failed");
	ret = mlockall(MCL_CURRENT | MCL_FUTURE);
	if (ret == -1)
		warn("mlockall() failed");
}

int main(int argc, char *argv[])
{
	long ms = 20;
	int dirty;
	struct timespec this;
	struct timespec next;
	int sleep;

	if (argc > 2)
		usage(argv[0]);
	if (argc == 2) {
		char *end;

		ms = strtol(argv[1], &end, 0);
		if (*end != '\0' || ms < 1)
			usage(argv[0]);
	}

	attempt_realtime();
	clock_gettime(CLOCK_REALTIME, &next);
	while (1) {
		clock_gettime(CLOCK_REALTIME, &this);
		add_ms(&next, ms);
		sleep = diff_ms(&next, &this);
		dirty = get_dirty();
		printf("%lu.%09lu: %d\n", this.tv_sec, this.tv_nsec, dirty);
		usleep(sleep * 1000);
	}
}
