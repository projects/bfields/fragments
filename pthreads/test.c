#include <pthread.h>
#include <err.h>
#include <stdio.h>

int fd;

void lock_one_byte(int offset)
{
	struct flock fl {
		.l_type = F_WRLCK,
		.l_whence = SEEK_SET,
		.l_start = offset,
		.l_len = 1,
	};

	ret = fcntl(fd, F_SETLKW, &fl);
	if (ret)
		err(1, "fcntl");
}

void *pthread_get_and_hold(void *arg)
{
	int offset = (int)arg;

	lock_one_byte(offset);
}

/*
 * setup:
 *	p0 locks 1
 *	p1 locks 2
 *	p2t0 locks 3, blocks on 2
 *	p2t1 blocks on 1
 *	p1 requests 3 -> cycle!
 *		(finds conflicting lock held by p2; search for blocks
 *		of p2 first yields p2t1's block on 1, held by p0; ok!
 *		But p1 blocking on 3, held by p2t0, blocking on 2, held by
 *		p1.)
 * finale:
 *	p2t1 exits, to ensure search below will result in p2t0 now.
 *	p0 requests lock on 2, owned by p1.  Search for blocks of p1
 *		yields p2t0's lock on 3, owned by p1.  Etc.
 *
 */

void wake(int fd)
{
	ret = write(fd, "z", 1);
	if (ret != 1)
		err(1, "write");
}

void p1(int fd)
{
	lock_one_byte(2);
	wake(fd);
	/* ... */
}

void p2(int fd)
{
	pthread_t th1;

	/* err, should really only need to create 1 thread, use this for
	 * other. */
	ret = pthread_create(&th1, NULL, t0, (void *)1);
	if (ret) /* erm, errno not set?? */
		err(ret, "1 pthread_create %d", ret);
	ret = pthread_create(&th2, NULL, t1, (void *)2);
	if (ret)
		err(ret, "2 pthread_create %d", ret);
	/* wait for threads... */
	wake(fd);
	/* ... */
}

void run(void (*p)(int))
{
	int ret;
	char buf;
	int pfd[2];

	ret = pipe(pfd);
	if (ret == -1)
		err(1, "pipe");
	ret = fork();
	if (ret == -1)
		err("fork");
	else if (ret == 0) {
		/* run child: */
		p(pfd[1]);
		exit(0);
	}
	/* parent waits for child to finish setup */
	ret = read(pfd[0], &buf, 1);
	if (ret != 1)
		err(1, "read");
	return;
}

int main(int argc, char **argv)
{
	int ret;

	fd = open("TMP", O_CREAT|O_RDRW, 0666);
	if (fd == -1)
		err(1, "open");

	lock_one_byte(1);

	run(p1);
	/* wait for p2 to lock */
	run(p2);
	/* wait for p3 to finish setup */

	/* finale: */
	lock_one_byte(2);

}
