#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>

void printnsec(long nsec)
{
	if (nsec == UTIME_NOW)
		printf("UTIME_NOW  ");
	else if (nsec == UTIME_OMIT)
		printf("UTIME_OMIT ");
	else
		printf("%10lu ", nsec);
}

int tryutime(char *p, long nseca, long nsecm)
{
	struct timespec times[2] = {
		{.tv_nsec = nseca},
		{.tv_nsec = nsecm}
	};
	int ret, err;

	ret = futimesat(AT_FDCWD, p, times, 0);
	err = errno;
	printnsec(nseca);
	printnsec(nsecm);
	printf("result: %d, %d\n", ret, errno);
}

int main(int argc, char *argv[])
{
	char *p = argv[1];

	tryutime(p, UTIME_NOW, UTIME_NOW);
	tryutime(p, UTIME_NOW, UTIME_OMIT);
	tryutime(p, UTIME_OMIT, UTIME_NOW);
	tryutime(p, UTIME_OMIT, UTIME_OMIT);
}
