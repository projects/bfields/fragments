#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>

char *comm;

void usage(void)
{
	errx(1, "usage: %s <source> <destination> "
			"[<len> [<off_in> [<off_out>]]]", comm);
}

long longorbust(char *str)
{
	long ret;
	char *ptr;

	ret = strtol(str, &ptr, 0);
	if (*ptr != '\0')
		usage();
	return ret;
}

off_t filesize(int fd)
{
	struct stat st;
	int ret;

	ret = fstat(fd, &st);
	if (ret == -1)
		err(1, "stat");
	return st.st_size;
}

int main(int argc, char *argv[])
{
	int fd_in, fd_out, ret;
	loff_t off_in = 0, off_out = 0;
	size_t len;

	comm = argv[0];

	if (argc < 3)
		usage();

	fd_in = open(argv[1], O_RDONLY);
	if (fd_in == -1)
		err(1, "open %s", argv[1]);
	fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);
	if (fd_out == -1)
		err(1, "open %s", argv[2]);

	if (argc >= 4)
		len	= longorbust(argv[3]);
	else
		len 	= filesize(fd_in);
	if (argc >= 5)
		off_in	= longorbust(argv[4]);
	if (argc >= 6)
		off_out	= longorbust(argv[5]);
	if (argc > 6)
		usage();

	ret = copy_file_range(fd_in, &off_in, fd_out, &off_out, len, 0);
	if (ret == -1)
		err(1, "copy");
	printf("copied %d bytes\n", ret);
	return 0;
}
