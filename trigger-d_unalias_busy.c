#include <sys/stat.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>

void usage(char *comm)
{
	errx(1, "usage: %s <path>", comm);
}

int main(int argc, char *argv[])
{
	struct stat s;
	int ret;

	if (argc != 2)
		usage(argv[0]);
	ret = chdir(argv[1]);
	if (ret)
		err(1, "unable to chdir to %s\n", argv[1]);
	ret = fork();
	if (ret == -1)
		err(1, "for");
	if (ret == 0) { /* child */
		mkdir("d", 0777);
		while (1) {
			rename("d", "e");
			rename("e", "d");
		}
	}
	while (1) {
		struct stat s;

		ret = stat("a/c", &s);
		if (ret && errno != ENOENT)
			err(1, "stat a/c");
		ret = stat("b/c", &s);
		if (ret && errno != ENOENT)
			err(1, "stat b/c");
	}
}
