#include <stdio.h>
#include <stdio_ext.h>
#include <unistd.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int i = 1;
	FILE *f;

	f = fopen(argv[1], "r+");
	if (f == NULL)
		err(1, "fopen");
	while (1) {
		fprintf(f, "%d\n", i);
		fflush(f);
		__fpurge(f);
		i++;
	}
}
