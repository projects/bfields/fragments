#include <attr/xattr.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <err.h>


int main(int argc, char *argv[])
{
	int fd;
	char *path;

	if (argc < 2)
		err(1, "usage: %s <path>", argv[0]);

	path = argv[1];

	/* This will fail, we're just curious about the error: */
	if (lremovexattr(path, "system.nfs4_acl") == -1)
		err(1, "lremovexattr");

	return 0;
}
