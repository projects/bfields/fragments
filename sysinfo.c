#include <sys/sysinfo.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char *argv[])
{
	struct sysinfo s;
	int ret;

	ret = sysinfo(&s);
	if (ret)
		errx(1, "sysinfo");
	printf("totalram %8lu\n", s.totalram);
	printf(" highram %8lu\n", s.totalhigh);
}
