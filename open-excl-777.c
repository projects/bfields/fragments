#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <err.h>

void usage (char *prog)
{
	errx(1, "usage: %s path", prog);
}

int main(int argc, char *argv[])
{
	int i, fd, ret;
	struct timeval tv;
	char *name;
	mode_t mask;

	if (argc != 2)
		usage(argv[0]);

	name = argv[1];

	mask = umask(0);
	printf("mask: %o\n", mask);
	umask(mask);

	fd = open(name, O_RDONLY | O_CREAT | O_EXCL | O_TRUNC, 0777);
	if (fd == -1)
		err(1, "open");
	ret = close(fd);
	if (ret)
		err(1, "close");
	exit(0);
}
