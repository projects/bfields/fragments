#include <stdlib.h>
#include <string.h>
#include <err.h>

void usage(char *comm)
{
	errx(1, "usage: %s <array size>", comm);
}

int main(int argc, char *argv[])
{
	long size;
	char *p;

	if (argc != 2)
		usage(argv[0]);
	size = strtol(argv[1], &p, 0);
	if (*p != '\0')
		usage(argv[0]);

	char a[size];
	memset(a, 1, sizeof(a));
}
