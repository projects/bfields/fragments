#!/bin/bash

mount=/mnt/puzzle/ext3
export=/exports/ext3

set -v
echo "stuff" >$export/testfile

tail -f $mount/testfile >/dev/null &

sleep 2

echo "more stuff" >>$export/testfile

cat $mount/testfile

kill %+

cat $mount/testfile
