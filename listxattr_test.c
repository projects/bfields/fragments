#include <sys/types.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char *argv[])
{
	char *p, buf[4096];
	ssize_t len;

	if (argc != 2)
		errx(1, "usage: %s path", argv[0]);
	len = listxattr(argv[1], buf, sizeof(buf));
	printf("len = %d\n", len);
	p = buf;
	while (len > 0) {
		printf("%s\n", p);
		len -= strlen(p) + 1;
		p += strlen(p) + 1;
	}
	printf("end of attribute list\n");
}
