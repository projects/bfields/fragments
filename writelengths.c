#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char *argv[])
{
	char *buf;
	int fd, ret, i, m, n;
	off_t s;

	if (argc != 4)
		errx(1, "usage: %0 <path> <minlength> <maxlength>", argv[0]);
	fd = open(argv[1], O_RDWR|O_SYNC);
	if (fd < 0)
		err(1, "open");
	m = atoi(argv[2]);
	n = atoi(argv[3]);
	buf = calloc(n, 1);
	if (!buf)
		err(1, "malloc");
	for (i = m; i <= n; i++) {
		ret = write(fd, buf, i);
		if (ret < 0)
			err(1, "write");
		s = lseek(fd, 0, SEEK_SET);
		if (s == -1)
			err(1, "lseek");
	}
	close(fd);
}
