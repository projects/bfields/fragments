#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <err.h>

int main(int argc, char **argv)
{
	int ret;

	if (argc != 3)
		errx(1, "usage: %s <from> <to>", argv[0]);
	ret = rename(argv[1], argv[2]);
	if (ret)
		err(1, "rename");
}
