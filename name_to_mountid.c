#define _GNU_SOURCE
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/syscall.h>

int fh_struct_size(struct file_handle *fh)
{
	return sizeof(struct file_handle) + fh->handle_bytes;
}

int main(int argc, char *argv[])
{
	struct file_handle dummyfh;
	struct file_handle *fh;
	char *name;
	int mtid;
	int ret;
	int fd;

	if (argc != 2)
		errx(1, "usage: %s <filename>", argv[0]);
	name = argv[1];

	fh->handle_bytes = 0;

	ret = name_to_handle_at(AT_FDCWD, name, &dummyfh, &mtid, 0);
	if (ret != -1)
		errx(1, "unexpected success\n");
	if (errno != EOVERFLOW)
		err(1, "name_to_handle_at to get length");

	/* dummyfh->handle_bytes is supposed to be set now. */

	fh = malloc(fh_struct_size(&dummyfh));
	if (!fh)
		errx(1, "allocating filehandle\n");
	fh->handle_bytes = dummyfh.handle_bytes;

	ret = name_to_handle_at(AT_FDCWD, name, fh, &mtid, 0);
	if (ret)
		err(1, "name_to_handle_at");
	printf("mountid: %d\n", mtid);
	exit(0);
}
