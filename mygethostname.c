#include <unistd.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char *argv[])
{
	char buf[4096];
	int ret;

	ret = gethostname(buf, sizeof(buf));
	if (ret)
		errx(1, "gethostname");
	printf("here you go: %s\n", buf);
	exit(0);
}
