#include <stdio.h>
#include <err.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

/* demonstrate that for mountpoints, readdir ino of mounted-on
 * directory, stat returns ino of mounted directory. */

int main(int argc, char *argv[])
{
	struct dirent *de;
	int ret;
	DIR *d;

	if (argc != 2)
		errx(1, "usage: %s <directory>", argv[0]);
	ret = chdir(argv[1]);
	if (ret)
		errx(1, "chdir /");
	d = opendir(".");
	if (!d)
		errx(1, "opendir .");
	while (de = readdir(d)) {
		struct stat st;

		ret = stat(de->d_name, &st);
		if (ret)
			errx(1, "stat %s", de->d_name);
		printf("%s %d %d\n", de->d_name, de->d_ino, st.st_ino);
	}
}
