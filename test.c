#include <pwd.h>
#include <sys/types.h>
#include <stdio.h>
#include <err.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	struct passwd *pw;

	if (argc != 2)
		errx(1, "need 1 integer argument");
	if ((pw = getpwuid(atoi(argv[1]))) == NULL)
		errx(1, "user with uid %s not found", argv[1]);
	printf("name: %s\n", pw->pw_name);
	exit(0);
}
