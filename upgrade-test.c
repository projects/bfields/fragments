#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <err.h>

void usage(char *prog)
{
	errx(1, "usage: %s <testfile>", prog);
}

int main(int argc, char *argv[])
{
	struct flock rdlock = { F_RDLCK, SEEK_SET, 0, 0, 0 };
	struct flock wrlock = { F_WRLCK, SEEK_SET, 0, 0, 0 };
	char buff[4096] = { 0 };
	int fd, ret;

	if (argc != 2)
		usage(argv[0]);
	fd = open(argv[1], O_RDONLY);
	if (fd == -1)
		err(1, "open");
	fcntl(fd, F_SETLK, &rdlock);

	fd = open(argv[1], O_RDWR);
	if (fd == -1)
		err(1, "open");
	ret = fcntl(fd, F_SETLK, &wrlock);
	if (ret)
		err(1, "fcntl");
	ret = write(fd, buff, sizeof buff);
	if (ret == -1)
		err(1, "write");
	ret = close(fd);
	if (ret)
		err(1, "close");
	return 0;
}

