#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

void print_times(char *str, struct stat *stat)
{
	printf("%s:\tctime:%d.%ld\tmtime:%d.%ld\n", str,
		stat->st_ctim.tv_sec, stat->st_ctim.tv_nsec,
		stat->st_mtim.tv_sec, stat->st_mtim.tv_nsec);
}

void stat_or_fail(char *p, struct stat *s)
{
	int ret;

	ret = stat(p, s);
	if (ret)
		err(1, "stat %s", p);
}

int main(int argc, char *argv[])
{
	int ret, fd;
	struct stat stat1, stat2, stat3;
	int delay;

	switch (argc) {
		case 1:
			delay = 0;
			break;
		case 2:
			delay = atoi(argv[1]);
			break;
		default:
			errx(1, "usage: %s [delay in microseconds]\n", argv[0]);
	}

	stat_or_fail(".", &stat1);

	ret = mkdir("testdir", 0x777);
	if (ret)
		err(1, "mkdir");
	stat_or_fail(".", &stat2);

	if (delay)
		usleep(delay);

	ret = rmdir("testdir");
	if (ret)
		err(1, "unlink");
	stat_or_fail(".", &stat3);

	printf("stat on current directory around mkdir/rmdir:\n");
	print_times("before mkdir", &stat1);
	print_times("after mkdir", &stat2);
	print_times("after rmdir", &stat3);

	fd = open("testfile", O_RDWR|O_CREAT, 0664);
	if (fd < 0)
		err(1, "open");
	stat_or_fail("testfile", &stat1);

	ret = write(fd, "X", 1);
	if (ret != 1)
		err(1, "write");
	stat_or_fail("testfile", &stat2);

	ret = write(fd, "Y", 1);
	if (ret != 1)
		err(1, "write");
	stat_or_fail("testfile", &stat3);

	ret = unlink("testfile");
	if (ret)
		err(1, "unlink");

	printf("stat on file around writes:\n");
	print_times("before writes", &stat1);
	print_times("between writes", &stat2);
	print_times("after writes", &stat3);

	exit(0);
}
