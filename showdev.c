#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <err.h>

int main(int argc, char *argv[])
{
	dev_t d;

	if (argc != 2)
		err(1, "usage: %s <n>", argv[0]);

	d = atoi(argv[1]);

	printf("makedev(%d, %d)\n", major(d), minor(d));
}
