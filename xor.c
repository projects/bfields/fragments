#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int f1, f2;

	if (argc != 3)
		errx(1, "need 2 pathnames");
	f1 = open(argv[1], O_RDONLY);
	if (f1 == -1)
		errx(1, "open %s\n", argv[1]);
	f2 = open(argv[2], O_RDONLY);
	if (f2 == -1)
		errx(1, "open %s\n", argv[2]);
	while (1) {
		unsigned char i1, i2, j;
		int ret;

		ret = read(f1, &i1, 1);
		if (ret != 1)
			exit(0);
		ret = read(f2, &i2, 1);
		if (ret != 1)
			errx(1, "second file was shorter?");
		j = i1 ^ i2;
		write(1, &j, 1);
	}
}
