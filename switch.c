#include <stdlib.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char *argv[])
{
	long int n;
	char *end;

	if (argc != 2)
		err(1, "usage: %s number", argv[0]);

	n = strtol(argv[1], &end, 0);

	switch (n) {
		default:
			printf("many!\n");
			break;
		case 1:
			printf("one!\n");
			break;
		case 2:
			printf("two!\n");
			break;
	}
	exit(0);
}
