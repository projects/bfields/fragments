#include <stdio.h>
#include <err.h>
#include <syslog.h>

int main(int argc, char *argv[])
{
	struct passwd *pw;

	if (argc != 2)
		err(1, "%usage: %s message\n");
	openlog("sent from commandline", 0, LOG_USER);
	syslog(LOG_NOTICE, argv[1]);
	closelog();
	exit(0);
}
