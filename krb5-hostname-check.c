#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <unistd.h>
#include <netdb.h>
#include <limits.h>

int main(int argc, char *argv[])
{
	char me[HOST_NAME_MAX + 1];
	char *input;
	struct hostent *he, *he2;
	int ret;

	switch (argc) {
	case 1:
		input = NULL;
		break;
	case 2:
		input = argv[1];
		break;
	default:
		errx(1, "usage: %s name\n", argv[0]);
	}

	ret = gethostname(me, sizeof(me));
	if (ret)
		err(1, "gethostname");
	printf("gethostname returns '%s'\n", me);

	he = gethostbyname(me);
	if (he == NULL)
		errx(1, "gethostbyname %d", h_errno);
	printf("gethostbyname: h_name = '%s'\n", he->h_name);
	if (input == NULL)
		exit(0);

	he = gethostbyname(input);
	if (he == NULL)
		errx(1, "gethostbyname(%s) %d", input, h_errno);
	printf("gethostbyname(%s) = '%s'\n", input, he->h_name);

	he2 = gethostbyaddr(he->h_addr, he->h_length, he->h_addrtype);
	if (he2 == NULL)
		errx(1, "gethostbyaddr(gethostbyname(%s) %d", input, h_errno);
	printf("gethostbyaddr(gethostbyname(%s)) = '%s'\n", input, he->h_name);
}
