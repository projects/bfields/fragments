#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int procs = 8;
	int reads_per_write = 8;
	int rsize = 4 * 4096;
	int wsize = rsize + 3 * 4096;
	int i;
	int fd;
	int ret;
	char rbuf[rsize];
	char buf[wsize];

	fd = open(argv[1], O_RDWR|O_CREAT|O_DIRECT, 0644);
	if (fd == -1)
		err(1, "open");
	i = 0;
	while (i < wsize) {
		ret =  snprintf(buf + i, sizeof(buf) - i, "%7d\n", i);
		if (ret < 0)
			err(1, "initializing buffer");
		i += ret;
	}
	ret = write(fd, buf, wsize);
	if (ret == -1)
		err(1, "initializing file\n");
	if (ret < wsize)
		errx(1, "short write initializing file\n");

	for (i = 0; i < procs - 1; i++) {
		ret = fork();
		if (ret == -1)
			err(1, "fork");
		if (ret)
			break;
	}
	/* do lotsa reads that cross page boundaries.  Make them big to
	 * increase time they spend sitting in outbound queues on
	 * server, and mix in some writes? */
	srand(i);

	while (1) {
		int r = rand() % 10;
		int offset = rand() % (wsize - rsize);

		if (r) {
			ret = pread(fd, rbuf, rsize, offset);
			if (ret == -1)
				err(1, "pread");
			if (ret < rsize)
				errx(1, "short read(.,.,%d, %d) = %d",
					rsize, offset, ret);
			if (memcmp(rbuf, buf + offset, rsize)) {
				fprintf(stderr, "corruption:\n");
				fprintf(stderr, "RECEIVED:\n");
				fprintf(stderr, "%s", rbuf);
				fprintf(stderr, "EXPECTED:\n");
				fprintf(stderr, "%s", buf + offset);
				errx(1, "corruption");
			}
		} else {
			ret = pwrite(fd, buf, wsize, 0);
			if (ret == -1)
				err(1, "write");
			if (ret < wsize)
				errx(1, "short write(.,.,%d, 0) = %d",
					wsize, ret);
		}
	}
}
