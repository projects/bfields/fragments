#include <stdio.h>
#include <string.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int ret;

	if (argc != 3)
		errx(1, "usage: %s string1 string2\n", argv[0]);
	ret = strcmp(argv[1], argv[2]);
	printf("strcmp('%s', '%s') = %d\n", argv[1], argv[2], ret);
}
