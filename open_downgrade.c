#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <err.h>
#include <fcntl.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	int fdrw, fdr, ret;

	if (argc != 2)
		errx(-1, "usage: %s <filename>\n");
	fdrw = open(argv[1], O_RDWR);
	if (fdrw == -1)
		errx(-1, "open O_RDWR");
	fdr = open(argv[1], O_RDONLY);
	if (fdr == -1)
		errx(-1, "open O_RDONLY");
	ret = close(fdrw);
	if (ret == -1)
		errx(-1, "close O_RDWR");
	ret = close(fdr);
	if (ret == -1)
		errx(-1, "close O_RDONLY");
	exit(0);
}
