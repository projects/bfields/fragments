#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int i, fd, ret;
	struct timeval tv;
	char name[10];

	for (i = 0; i < 2000000; i++) {
		sprintf(name, "%d", i);
		fd = open(name, O_RDONLY | O_CREAT, 0644);
		if (fd == -1)
			err(1, "open");
		ret = close(fd);
		if (ret)
			err(1, "close");
		if (i % 100 == 0) {
			struct timeval tmp;
			float t;
			ret = gettimeofday(&tmp, NULL);
			if (ret)
				err(1, "gettimeofday");
			if (i != 0) {
				t = tmp.tv_sec - tv.tv_sec
					+ (tmp.tv_usec - tv.tv_usec)
								/ 1000000.0;
				printf("%d: %f\n", i, t);
			}
			tv = tmp;
		}
	}
}
