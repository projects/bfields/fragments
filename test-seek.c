#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int fd;
	off_t off;

	fd = open(argv[1], O_RDONLY);
	if (fd == -1)
		err(1, "open %s", argv[1]);
	off = lseek(fd, 0, SEEK_HOLE);
	off = lseek(fd, 0, SEEK_DATA);
}
