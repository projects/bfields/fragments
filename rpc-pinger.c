#include <rpc/xdr.h>
#include <rpc/rpc.h>
#include <err.h>

#define NFS_PROGRAM       (100003)

int main(int argc, char *argv[])
{
	char *server;
	CLIENT *c;
	enum clnt_stat s;
	static struct timeval timeout = { .tv_sec = 30 };
	int procs;
	pid_t p;

	if (argc != 3)
		errx(1, "usage: %s <num processes> <host>", argv[0]);
	procs = atoi(argv[1]);
	server = argv[2];
	if (procs > (1<<20) || procs < 0)
		errx(1, "bad number of processes %s\n", argv[1]);
child:
	if (procs > 1) {
		procs--;
		p = fork();
		if (p < 0)
			err(1, "fork");
		if (p == 0)
			goto child;
	}
	c = clnt_create(server, NFS_PROGRAM, 4, "tcp");
	if (!c)
		errx(1, "failed to create client: %s", clnt_spcreateerror(""));
	while (1) {
		s = clnt_call(c, 0, (xdrproc_t)xdr_void, NULL,
				    (xdrproc_t)xdr_void, NULL, timeout);
		if (s)
			errx(1, "%s", clnt_sperrno(s));
	}
}
