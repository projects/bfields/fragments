#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <err.h>

void usage(char *name)
{
	printf("%d file [wsize [numwrites]]\n", name);
	exit(1);
}

int main(int argc, char *argv[])
{
	int wsize = 16 * 4096;
	int nwrites = 1;
	int fd;
	int ret;
	char *ptr;
	char *buf;

	switch (argc) {
		case 4:
			nwrites = strtol(argv[3], &ptr, 0);
			if (*ptr != '\0')
				usage(argv[0]);
		case 3:
			wsize = strtol(argv[2], &ptr, 0);
			if (*ptr != '\0')
				usage(argv[0]);
		case 2:
			break;
		default:
			usage(argv[0]);
	}
	buf = calloc(wsize, 1);
	if (!buf)
		err(1, "calloc");
	fd = open(argv[1], O_RDWR|O_CREAT|O_DIRECT, 0644);
	if (fd == -1)
		err(1, "open");
	while (nwrites--) {
		ret = write(fd, buf, wsize);
		if (ret == -1)
			err(1, "write\n");
		printf("write returned %d\n", ret);
	}
}
