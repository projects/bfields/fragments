#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	char buf[4096];
	printf("called as %s\n", argv[0]);
	readlink(argv[0], buf, sizeof(buf));
	printf(" (->%s)\n", buf);
	while (1) {
		readlink("/proc/self/exe", buf, sizeof(buf));
		printf(" == %s\n", buf);
		sleep(1);
	}
}
