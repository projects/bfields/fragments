#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int ret;
	int i;

	for (i=1; i <= 4096; i++) {
		ret = mkdir("foo", 0777);
		if (ret == -1)
			err(1, "mkdir i=%d", i);
		ret = chdir("foo");
		if (ret == -1)
			err(1, "chdir i=%d", i);
	}
}
