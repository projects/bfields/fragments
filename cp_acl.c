#include <sys/types.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char *argv[])
{
	char buf[4096];
	int ret;

	if (argc != 3)
		err(1, "usage: %s source_path dest_path", argv[0]);

	ret = getxattr(argv[1], "system.nfs4_acl", buf, sizeof(buf));
	if (ret < 0)
		err(1, "getxattr on %s", argv[1]);
	ret = setxattr(argv[2], "system.nfs4_acl", buf, ret, 0);
	if (ret < 0)
		err(1, "setxattr on %s", argv[2]);
	exit(ret);
}
