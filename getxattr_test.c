#include <sys/types.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	char buf[4096];
	int ret;

	if (argc != 3)
		errx(1, "usage: %s path key", argv[0]);
	
	ret = getxattr(argv[1], argv[2], buf, sizeof(buf));
	if (ret < 0) {
		printf("getxattr: %s\n", strerror(ret));
		exit(ret);
	}
	printf("value: %.*s\n", ret, buf);
	exit(ret);
}
