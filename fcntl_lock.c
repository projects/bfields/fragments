#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <err.h>
#include <string.h>

void usage(char *prog)
{
		errx(1, "usage: %s r|w file_to_lock [start len]", prog);
}

int main(int argc, char *argv[])
{
	char buf[4096];
	char * file;
	struct flock fl;
	struct timeval pre, post;
	int fd, ret, flags = O_CREAT;
	short ltype;
	char *ptr;
	off_t start = 0;
	off_t len = 0;

	if (argc != 3 && argc != 5)
		usage(argv[0]);

	if (!strcmp(argv[1], "r")) {
		flags |= O_RDONLY;
		ltype = F_RDLCK;
	} else if (!strcmp(argv[1], "w")) {
		flags |= O_RDWR;
		ltype = F_WRLCK;
	} else
		usage(argv[0]);
	file = argv[2];

	if (argc == 5) {
		start = strtol(argv[3], &ptr, 0);
		if (*ptr != '\0')
			usage(argv[0]);
		len = strtol(argv[4], &ptr, 0);
		if (*ptr != '\0')
			usage(argv[0]);
	}

	fd = open(file, flags, 0666);
	if (fd == -1)
		err(1, "open");
	/* lock on whole file: */
	fl.l_type = ltype;
	fl.l_whence = SEEK_SET;
	fl.l_start = start;
	fl.l_len = len;
	gettimeofday(&pre, NULL);
	ret = fcntl(fd, F_SETLKW, &fl);
	gettimeofday(&post, NULL);
	if (ret == -1)
		err(1, "fcntl(fd, F_SETLKWD, fl)");
	printf("got lock in %ld.%06ld seconds; waiting\n",
			post.tv_sec - pre.tv_sec,
			post.tv_usec- pre.tv_usec);
	select(0, NULL, NULL, NULL, NULL);
	exit(0);
}
