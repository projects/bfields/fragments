#include <stdio.h>

#define PAGE_SHIFT 12

#define PAGE_SIZE (1UL << PAGE_SHIFT)

#define PAGE_MASK (~(PAGE_SIZE-1))

int main(int argc, char *argv[])
{
	unsigned long end = 3296;
	unsigned long page_end = (end + ~PAGE_MASK) & PAGE_MASK;

	printf("end %ld page_end %ld\n", end, page_end);
}
