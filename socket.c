#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

/* XXX: unfinished and buggy.*/

void worker(int in, int out)
{
	int ret;
	char buf[1];

	while (1) {
		ret = read(in, buf, sizeof(buf));
		if (ret == -1)
			err(1, "read");
		if (ret == 0)
			exit(0);
		ret = write(out, buf, ret);
		if (ret == -1)
			errx(1, "write");
	}
}

void listener(struct sockaddr_un *una)
{
	struct sockaddr *a = (struct sockaddr *)una;
	int fd;
	int opt = 1;
	int ret;

	fd = socket(PF_LOCAL, SOCK_STREAM, 0);
	if (fd == -1)
		err(1, "socket");
	/* Note: this doesn't actually do anything: */
	ret = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));
	if (ret == -1)
		err(1, "setsockopt");
	ret = bind(fd, a, SUN_LEN(una));
	if (ret == -1)
		err(1, "bind");
	ret = listen(fd, 1);
	if (ret == -1)
		err(1, "listen");
	while (1) {
		struct sockaddr_un client_addr;

		socklen_t client_addrlen;
		int cfd, ret;
		pid_t child;
		char buf[100];

		cfd = accept(fd, (struct sockaddr *)&client_addr, &client_addrlen);
		if (cfd == -1)
			err(1, "accept");
		printf("accepted\n");
		child = fork();
		if (child == -1)
			err(1, "fork");
		if (child == 0)
			worker(cfd, 1);
	}
}

void usage(char *name)
{
	errx(1, "usage: %s [c|s] socketname", name);
}

void client(struct sockaddr_un *una)
{
	struct sockaddr *a = (struct sockaddr *)una;
	int fd;
	int ret;

	fd = socket(PF_LOCAL, SOCK_STREAM, 0);
	if (fd == -1)
		err(1, "socket");
	ret = connect(fd, a, SUN_LEN(una));
	if (ret == -1)
		err(1, "connect");
	printf("connected\n", ret);
	worker(0, fd);
}

int main(int argc, char *argv[])
{
	char *path;
	struct sockaddr_un a;
	int ret;

	if (argc != 3)
		usage(argv[0]);
	path = argv[2];
	if (strlen(path) + 1 > sizeof(a.sun_path))
		errx(1, "'%s' is too long", path);
	a.sun_family = AF_LOCAL;
	strcpy(a.sun_path, path);
	if (ret == -1)
		err(1, "bind");
	switch (argv[1][0]) {
		case 'c':
			client(&a);
		case 's':
			listener(&a);
		default:
			usage(argv[0]);
	}
}
