#include <pthread.h>
#include <err.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>

int pfd[2];
int glibc = 0;
int fsuid = 0;

void *t1(void *arg)
{
	write(pfd[1], "z", 1);
	if (glibc) {
		if (fsuid)
			setfsuid(2815);
		else
			setuid(2815);
	} else {
		if (fsuid)
			syscall(SYS_setfsuid, 2815);
		else
			syscall(SYS_setuid, 2815);
	}
	creat("foo", 0666);
	write(pfd[1], "z", 1);
}

int main(int argc, char *argv[])
{
	pthread_t th;
	char buf;
	int i = 0;

	while (++i < argc) {
		if (!strcmp(argv[i], "glibc"))
			glibc = 1;
		else if (!strcmp(argv[i], "fsuid"))
			fsuid = 1;
		else
			errx(1, "%s [glibc] [fsuid]", argv[0]);
	}

	pipe(pfd);
	unlink("foo");
	unlink("bar");

	pthread_create(&th, NULL, t1, NULL);
	read(pfd[0], &buf, 1);
	read(pfd[0], &buf, 1);
	creat("bar", 0666);
}
