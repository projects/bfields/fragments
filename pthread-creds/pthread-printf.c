#include <pthread.h>
#include <err.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>

void *t1(void *arg)
{
	int i;
	for (i=0; i<400; i++)
		printf("ABCDEFGHIJKLMNOPQRSTUVWXYZ+++++++++++TICK-\n"
		       "ABCDEFGHIJKLMNOPQRSTUVWXYZ+++++++++++tock\n"
		       "ABCDEFGHIJKLMNOPQRSTUVWXYZ+++++++++++tock\n"
		       "ABCDEFGHIJKLMNOPQRSTUVWXYZ+++++++++++tock\n");
}

int main(int argc, char *argv[])
{
	pthread_t th;
	int i;

	pthread_create(&th, NULL, t1, NULL);
	for (i=0; i<400; i++)
		printf("ABCDEFGHIJKLMNOPQRSTUVWXYZ-----------HEYY-\n"
		       "ABCDEFGHIJKLMNOPQRSTUVWXYZ-----------hooo\n"
		       "ABCDEFGHIJKLMNOPQRSTUVWXYZ-----------hooo\n"
		       "ABCDEFGHIJKLMNOPQRSTUVWXYZ-----------hooo\n");
	pthread_join(th, NULL);
}
